/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putstr_non_printable.c                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/04 18:59:58 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/07 14:54:15 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void	ft_nextchar_hex(unsigned int nb)
{
	const char		hex[17] = "0123456789abcdef";
	unsigned char	r;

	if (nb != 0)
	{
		r = hex[nb % 16];
		nb = nb / 16;
		ft_nextchar_hex(nb);
		write(1, &r, 1);
	}
}

void	ft_putnbr_hex(unsigned int nb)
{
	write(1, "\\", 1);
	if (nb == 0)
		write(1, "00", 2);
	else if (nb >= 16)
		ft_nextchar_hex(nb);
	else
	{
		write(1, "0", 1);
		ft_nextchar_hex(nb);
	}
}

int		ft_is_printable(char c)
{
	return (c >= ' ' && c <= '~');
}

void	ft_putstr_non_printable(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (ft_is_printable(str[i]))
			write(1, &str[i], 1);
		else
			ft_putnbr_hex((unsigned char)str[i]);
		i++;
	}
}
