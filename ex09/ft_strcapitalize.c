/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strcapitalize.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/03 23:06:35 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/05 10:59:29 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int		is_lowercase(char c)
{
	return (c >= 'a' && c <= 'z');
}

int		is_uppercase(char c)
{
	return (c >= 'A' && c <= 'Z');
}

int		is_numeric(char c)
{
	return (c >= '0' && c <= '9');
}

int		is_alphanumeric(char c)
{
	return (is_lowercase(c) || is_uppercase(c) || is_numeric(c));
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (is_lowercase(str[i]) && ((i == 0) | !is_alphanumeric(str[i - 1])))
		{
			str[i] -= 'a' - 'A';
		}
		else if (is_uppercase(str[i]) && is_alphanumeric(str[i - 1]))
		{
			str[i] += 'a' - 'A';
		}
		i++;
	}
	return (str);
}
