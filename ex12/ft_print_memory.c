/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_memory.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/05 10:04:38 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/07 19:45:03 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

static void		ft_nextchar_hex(long nb, unsigned int len)
{
	const char		hex[17] = "0123456789abcdef";
	char			r;

	if (nb != 0)
	{
		r = hex[nb % 16];
		nb = nb / 16;
		len--;
		ft_nextchar_hex(nb, len);
		write(1, &r, 1);
	}
	else
	{
		while (len--)
			write(1, "0", 1);
	}
}

static void		ft_putnbr_hex(long nb, unsigned int len)
{
	unsigned int i;

	i = 0;
	if (nb == 0)
	{
		while (i < len)
		{
			write(1, "0", 1);
			i++;
		}
	}
	else if (nb >= 16)
		ft_nextchar_hex(nb, len);
	else
	{
		ft_nextchar_hex(nb, len);
	}
}

static void		ft_print_chars(unsigned char *addr, unsigned int n)
{
	const unsigned char	*start = addr;

	while (addr - start < n)
	{
		if (*addr >= ' ' && *addr <= '~')
			write(1, addr, 1);
		else
			write(1, ".", 1);
		addr++;
	}
}

static void		ft_print_bytes(unsigned char *addr, unsigned int n)
{
	const unsigned char *start = addr;

	while (addr - start < 16)
	{
		if (addr - start < n)
			ft_putnbr_hex(*addr, 2);
		else
			write(1, "  ", 2);
		if ((addr - start) % 2)
			write(1, " ", 1);
		addr++;
	}
}

void			*ft_print_memory(void *addr, unsigned int size)
{
	const unsigned char	*start = addr;
	unsigned char		*addr_char;
	int					bytes_count;
	int					bytes_left;

	bytes_left = size;
	addr_char = addr;
	while (addr_char - start < size)
	{
		ft_putnbr_hex((long)addr_char, 15);
		write(1, ": ", 2);
		if (bytes_left <= 16)
			bytes_count = bytes_left;
		else
		{
			bytes_count = 16;
			bytes_left -= 16;
		}
		ft_print_bytes(addr_char, bytes_count);
		ft_print_chars(addr_char, bytes_count);
		write(1, "\n", 1);
		addr_char += 16;
	}
	return (addr);
}
